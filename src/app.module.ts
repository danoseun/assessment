import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
//import { CompaniesController } from './companies/companies.controller';
import { MongooseModule } from '@nestjs/mongoose';
//import { CompaniesService } from './companies/companies.service';
import { CompaniesModule } from './companies/companies.module';
//import { ReportsController } from './reports/reports.controller';
//import { ReportsService } from './reports/reports.service';
import { ReportsModule } from './reports/reports.module';
import config from './config/keys';

@Module({
  imports: [CompaniesModule, ReportsModule, MongooseModule.forRoot(config.mongoURI, { useNewUrlParser: true }), ReportsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

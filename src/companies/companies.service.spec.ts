import { Test, TestingModule } from '@nestjs/testing';
import { CompaniesController } from './companies.controller';
import { CompaniesService } from './companies.service';
import { Company } from './interfaces/companies.interface';
import { CreateCompanyDto } from './dto/create-company.dto';
import { Body } from '@nestjs/common';

describe('Company Controller', () => {
  let companiesController: CompaniesController;
  let companiesService: CompaniesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CompaniesController],
      providers: [CompaniesService],
    }).compile();

    companiesService = module.get<CompaniesService>(CompaniesService);
    companiesController = module.get<CompaniesController>(CompaniesController);
  });

  describe('Company', () => {
    it('findAll should return an array of Companys', async () => {
      let result: Promise<Company[]>
      jest.spyOn(companiesService, 'getCompanies').mockImplementation(() => result);

      expect(await companiesController.getCompanies('res')).toBe(result);
    });

    it('create should return a Company', async () => {
      let result: Promise<Company>
      let dto = new CreateCompanyDto();
      jest.spyOn(companiesService, 'addCompany').mockImplementation(() => result);

      expect(await companiesController.addCompany(dto, @Body()).toBe(result)
    });

    it('findOne should return an Company found', async () => {
      let result: Promise<Company>
      jest.spyOn(companiesService, 'getCompany').mockImplementation(() => result);

      expect(await companiesController.getCompany('id', 'any')).toBe(result);
    });
  });















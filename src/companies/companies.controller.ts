import { Controller, Get, Post, Res, HttpStatus, NotFoundException, Body, Param } from '@nestjs/common';
import { CreateCompanyDto } from './dto/create-company.dto';
import { CompaniesService } from './companies.service';
import { ValidateObjectId } from '../shared/pipes/validate-object-id.pipes';

@Controller('companies')
export class CompaniesController {
    constructor(private readonly companiesService: CompaniesService) {}
    @Get()
    async getCompanies(@Res() res) {
        const companies = await this.companiesService.getCompanies();
        return res.status(HttpStatus.OK).json(companies);
    }

    @Get(':id')
    async getCompany(@Res() res, @Param('id', new ValidateObjectId()) id) {
        const company = await this.companiesService.getCompany(id);
        if (!company) throw new NotFoundException('Company does not exist!');
        return res.status(HttpStatus.OK).json(company);
    }

    @Post()
    addCompany(@Res() res, @Body() createCompanyDTO: CreateCompanyDto) {
        const newCompany = this.companiesService.addCompany(createCompanyDTO);
        return res.status(HttpStatus.OK).json({
            message: "Company has been created successfully!",
            company: newCompany
        })
    }
}

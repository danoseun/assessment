export class CreateCompanyDto {
    readonly name: string;
    readonly address: string;
    readonly email: string;
    readonly description: string;
    readonly reports: string
}
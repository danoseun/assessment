import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Company } from './interfaces/companies.interface';
import { CreateCompanyDto } from './dto/create-company.dto';

@Injectable()
export class CompaniesService {
    constructor(@InjectModel('Company') private readonly companyModel: Model<Company>) {}

    async addCompany(createCompanyDTO: CreateCompanyDto): Promise<Company> {
        const newCompany = await this.companyModel(createCompanyDTO);
        return newCompany.save();
    }

    async getCompanies(): Promise<Company[]> {
        const companies = await this.companyModel.find().exec();
        return companies;
    }

    async getCompany(id: string): Promise<Company> {
        const company = await this.companyModel
            .findOne({ _id: id})
            .exec();
        return company;
    }
}

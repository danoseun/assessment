export class CreateReportDto {
    readonly name: string;
    readonly type: string;
    readonly period: string;
    readonly year: number;
    readonly assignee: string
    readonly deadline: string
    readonly submitted: boolean
    readonly url: string
}
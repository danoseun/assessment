import { Test, TestingModule } from '@nestjs/testing';
import { ReportsController } from './reports.controller';
import { ReportsService } from './reports.service';
import { Report } from './interfaces/reports.interface';
import { CreateReportDto } from './dto/create-report.dto';
import { Body } from '@nestjs/common';

describe('Report Controller', () => {
  let reportsController: ReportsController;
  let reportsService: ReportsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReportsController],
      providers: [ReportsService],
    }).compile();

    reportsService = module.get<ReportsService>(ReportsService);
    reportsController = module.get<ReportsController>(ReportsController);
  });

  describe('Company', () => {
    it('findAll should return an array of Companys', async () => {
      let result: Promise<Report[]>
      jest.spyOn(reportsService, 'getReports').mockImplementation(() => result);

      expect(await reportsController.getReports('res')).toBe(result);
    });

    it('create should return a Company', async () => {
      let result: Promise<Report>
      let dto = new CreateReportDto();
      jest.spyOn(reportsService, 'addReport').mockImplementation(() => result);

      expect(await reportsController.addReport(dto, Body()).toBe(result)
    });

    it('findOne should return an Company found', async () => {
      let result: Promise<Report>
      jest.spyOn(reportsService, 'getReport').mockImplementation(() => result);

      expect(await reportsController.getReport('id', 'any')).toBe(result);
    });
  });















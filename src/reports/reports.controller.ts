import { Controller, Get, Post, Res, HttpStatus, NotFoundException, Body, Param } from '@nestjs/common';
import { CreateReportDto } from './dto/create-report.dto';
import { ReportsService } from './reports.service';
import { ValidateObjectId } from '../shared/pipes/validate-object-id.pipes';

@Controller('reports')
export class ReportsController {
    constructor(private readonly reportsService: ReportsService) {}
    @Get()
    async getReports(@Res() res) {
        const reports = await this.reportsService.getReports();
        return res.status(HttpStatus.OK).json(reports);
    }

    @Get(':id')
    async getReport(@Res() res, @Param('id', new ValidateObjectId()) id) {
        const report = await this.reportsService.getReport(id);
        if (!report) throw new NotFoundException('Report does not exist!');
        return res.status(HttpStatus.OK).json(report);
    }

    @Post()
    addReport(@Res() res, @Body() createReportDTO: CreateReportDto) {
        const newReport = this.reportsService.addReport(createReportDTO);
        return res.status(HttpStatus.OK).json({
            message: "Report has been created successfully!",
            report: newReport
        })
    }
}

import * as mongoose from 'mongoose';

export const ReportSchema = new mongoose.Schema({
    name: String,
    type: String,
    period: String,
    year: Number,
    assignee: String,
    deadline: String,
    submitted: Boolean,
    url: String
})
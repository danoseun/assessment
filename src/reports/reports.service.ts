import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Report } from './interfaces/reports.interface';
import { CreateReportDto } from './dto/create-report.dto';

@Injectable()
export class ReportsService {
    constructor(@InjectModel('Report') private readonly ReportModel: Model<Report>) {}

    async addReport(createReportDTO: CreateReportDto): Promise<Report> {
        const newReport = await this.ReportModel(createReportDTO);
        return newReport.save();
    }

    async getReports(): Promise<Report[]> {
        const companies = await this.ReportModel.find().exec();
        return companies;
    }

    async getReport(id: string): Promise<Report> {
        const Report = await this.ReportModel
            .findOne({ _id: id})
            .exec();
        return Report;
    }
}
